nz_cities = [  # all have to be one word
    "auckland",
    "christchurch",
    "wellington",
    "hamilton",
    "tauranga",
    "dunedin",
    "palmerston",
    "napier",
    "plymouth",
    "rotorua",
    "whangarei",
    "nelson",
    "hastings",
    "invercargill",
    "whanganui",
    "gisborne",
    "hutt",
    "porirua",
    "queenstown",
    "greymouth",
    "westport",
    "hokitika"
]

nz_regions = [  # ignoring places with a name already in the cities list. all have to be one word
    "northland",
    "plenty",  # bay of plenty
    "waikato",
    "taranaki",
    "manawatu",
    "wanganui",
    "hawke's",
    "marlborough",
    "tasman",
    # "west",  too many issues. will assume greymouth, westport or hokitika as viable and add to list
    "canterbury",
    "otago",
    "southland",
    "chatham",
    "zealand",
    "aotearoa",
    "nz",
    "nz_emoji"
]

nz_emoji = "🇳🇿"

def isTwitterActorFromNewZealand(cached_twitter_actor):
    if not cached_twitter_actor.location:
        return None
    else:
        location_words = cached_twitter_actor.location.lower().replace(",", " ").split()
        for word in location_words:
            if word in nz_cities:
                return word
            if word in nz_regions:
                return word
            if nz_emoji in word:
                return "nz_emoji"
        if nz_emoji in cached_twitter_actor.name:
            return "nz_emoji"
        return None


if __name__ == "__main__":
    import pickle
    from file_loc import user_info_dir
    import os

    nz_probably = {x: list() for x in [*nz_cities, *nz_regions]}
    unconfirmed = list()
    for user_pkl in os.listdir(user_info_dir):
        with open(os.path.join(user_info_dir, user_pkl), "rb") as fi:
            user = pickle.load(fi)
        loc = isTwitterActorFromNewZealand(user)
        if loc:
            nz_probably[loc].append(user)
        else:
            unconfirmed.append(user)

    print("PROBABLY NZ")
    for k, v in nz_probably.items():
        if not len(v):
            continue
        print(f"{k} ({len(v)}):")
        for vv in sorted([(q.handle, len(q.getMutuals()), len(q.followers) == q.follower_count and len(q.following) == q.following_count, q.name) for q in v], key=lambda z: z[1], reverse=True):
            print(f"\t{vv[0]} - {vv[1]}{'' if vv[2] else '*'} | {vv[3]}")


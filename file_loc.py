import os

if os.name == "nt":
    pickle_dir = "F:\\_data\\socials"
    user_info_dir = "F:\\_data\\socials\\users"
    followers_dir = "F:\\_data\\socials\\followers"
    following_dir = "F:\\_data\\socials\\following"
else:
    pickle_dir = "/opt/twitterbot/data/socials/"
    user_info_dir = "/opt/twitterbot/data/socials/users"
    followers_dir = "/opt/twitterbot/data/socials/followers"
    following_dir = "/opt/twitterbot/data/socials/following"


class CachedTwitterActor(object):
    def __init__(self, name, id, handle, follower_count, following_count, tweet_count, location, protected, followers=set(), following=set()):
        self.name = name
        self.id = str(id)
        self.handle = handle
        self.follower_count = int(follower_count)
        self.following_count = int(following_count)
        self.tweet_count = int(tweet_count)
        self.location = location
        self.protected = protected
        self.followers = followers  # set of ids
        self.following = following  # set of ids

    def getMutuals(self):
        return self.followers.intersection(self.following)


def makeCachedTwitterActorFromResponse(response):
    return CachedTwitterActor(
        response.data.name,
        response.data.id,
        response.data.username,
        response.data.public_metrics["followers_count"],
        response.data.public_metrics["following_count"],
        response.data.public_metrics["tweet_count"],
        response.data.location,
        response.data.protected
    )

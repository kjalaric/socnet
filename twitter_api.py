import os
import time
import pickle
import tweepy
from classes import *
from globals import known_actors
from file_loc import *
from classes import CachedTwitterActor, makeCachedTwitterActorFromResponse
from secrets import *
import datahandling


client = tweepy.Client(bearer_token=bearer_token, wait_on_rate_limit=True)


class TwitterAPIGenericException(Exception):
    pass


def getSingleUser(_id, dont_use_api=False):
    cachefile = os.path.join(user_info_dir, f"{_id}.pkl")
    if os.path.exists(cachefile):
        with open(cachefile, "rb") as fi:
            output = pickle.load(fi)
        return output
    else:
        incomplete_cachefile = os.path.join(user_info_dir, f"{_id}_incomplete.pkl")
        if os.path.exists(incomplete_cachefile):
            with open(incomplete_cachefile, "rb") as fi:
                output = pickle.load(fi)
            return output
        else:
            if not dont_use_api:
                output = makeCachedTwitterActorFromResponse(client.get_user(id=_id, user_fields=["public_metrics", "location", "protected"]))
                with open(incomplete_cachefile, "wb") as fo:
                    pickle.dump(output, fo)
                return output
            else:
                raise TwitterAPIGenericException("Didn't use API, couldn't find it locally.")


def loadUserInfoIntoKnownActors(_id, dont_use_api=False):
    known_actors[_id] = getSingleUser(_id, dont_use_api)


def handleFromId(_id, dont_use_api=False, return_id_on_failure=False):
    try:
        return known_actors[_id].handle
    except KeyError:
        try:
            loadUserInfoIntoKnownActors(_id, dont_use_api)
            return known_actors[_id].handle
        except TwitterAPIGenericException:
            if return_id_on_failure:
                return _id
            else:
                raise


def getUserFollowers(_id):
    """
    user followers are stored as sets of ids
    :param id:
    :return:
    """
    cachefile = os.path.join(followers_dir, f"{_id}.pkl")
    if os.path.exists(cachefile):
        with open(cachefile, "rb") as fi:
            followers = pickle.load(fi)
        for x in followers:
            known_actors[str(_id)] = getSingleUser(x)
    else:
        next_token = None
        followers = set()
        while True:
            response = client.get_users_followers(_id, pagination_token=next_token)
            for x in response.data:
                followers.add(str(x.id))
            if "next_token" in response.meta.keys():
                next_token = response.meta["next_token"]
            else:
                break
        with open(cachefile, "wb") as fo:
            pickle.dump(followers, fo)
    return followers


def getUserFollowing(_id):
    """
    user following are stored as sets of ids
    :param id:
    :return:
    """
    cachefile = os.path.join(following_dir, f"{_id}.pkl")
    if os.path.exists(cachefile):
        with open(cachefile, "rb") as fi:
            following = pickle.load(fi)
        for x in following:
            known_actors[str(_id)] = getSingleUser(x)
    else:
        next_token = None
        following = set()
        while True:
            response = client.get_users_following(_id, pagination_token=next_token)
            for x in response.data:
                following.add(str(x.id))
            if "next_token" in response.meta.keys():
                next_token = response.meta["next_token"]
            else:
                break
        with open(cachefile, "wb") as fo:
            pickle.dump(following, fo)
    return following


def completeCacheFile(_id, return_on_too_many_requests=False):
    cachefile = datahandling.userInfoCacheFile(_id)

    if os.path.exists(cachefile):
        with open(cachefile, "rb") as fi:
            actor = pickle.load(fi)
        return actor


    incomplete_cachefile = datahandling.userInfoCacheFileIncomplete(_id)

    with open(incomplete_cachefile, "rb") as fi:
        actor = pickle.load(fi)

    if not actor.protected:
        if actor.follower_count:
            while True:
                try:
                    followers = getUserFollowers(_id)
                    break
                except tweepy.errors.TooManyRequests:
                    if return_on_too_many_requests:
                        return
                    else:
                        print("Hit rate limit, waiting for 15 minutes...")
                        time.sleep(60 * 15 + 3)
            actor.followers = followers

        if actor.following_count:
            while True:
                try:
                    following = getUserFollowing(_id)
                    break
                except tweepy.errors.TooManyRequests:
                    if return_on_too_many_requests:
                        return
                    else:
                        print("Hit rate limit, waiting for 15 minutes...")
                        time.sleep(60 * 15 + 3)
            actor.following = following

    with open(cachefile, "wb") as fo:
        pickle.dump(actor, fo)

    os.remove(incomplete_cachefile)
    return actor

import os
import pickle
from globals import known_actors
from file_loc import *


def userInfoCacheFile(_id):
    return os.path.join(user_info_dir, f"{_id}.pkl")


def userInfoCacheFileIncomplete(_id):
    return os.path.join(user_info_dir, f"{_id}_incomplete.pkl")


def loadAllKnownActors():
    for actor in os.listdir(user_info_dir):
        with open(os.path.join(user_info_dir, actor), "rb") as fi:
            actor_dict = pickle.load(fi)
        known_actors[actor.split(".")[0]] = actor_dict


def loadActor(_id):
    try:
        with open(userInfoCacheFile(_id), "rb") as fi:
            actor_dict = pickle.load(fi)
    except FileNotFoundError:
        with open(userInfoCacheFileIncomplete(_id), "rb") as fi:
            actor_dict = pickle.load(fi)

    known_actors[_id] = actor_dict

import os
import time
import pickle
import tweepy
from datahandling import *
from twitter_api import *
from classes import *
from globals import known_actors
from secrets import origin_id
import geo


if __name__ == "__main__":
    origin_actor = getSingleUser(origin_id)
    origin_actor = completeCacheFile(origin_id)

    actors_to_scrape = origin_actor.followers.union(origin_actor.following)
    actors_to_look_at = len(actors_to_scrape)

    # get incomplete cache files for all
    ids_vs_connected_ids = list()
    for actor_id in actors_to_scrape:
        try:
            actor = getSingleUser(actor_id)
        except tweepy.errors.TooManyRequests:
            break
            print("Hit rate limit, waiting for 15 minutes...")
            time.sleep(60 * 15 + 3)
        ids_vs_connected_ids.append((actor.id, actor.following_count + actor.follower_count, geo.isTwitterActorFromNewZealand(actor)))

    # sort so we look at smaller ones first, also those from NZ
    nzers = sorted([(x[0], x[1]) for x in ids_vs_connected_ids if x[2] is not None], key=lambda x: x[1])
    not_nzers = sorted([(x[0], x[1]) for x in ids_vs_connected_ids if x[2] is None], key=lambda x: x[1])

    # remove people in not_nzers that have geo information indicating they're not in NZ, i.e. not blank (we assume they've been picked up otherwise)
    removed = len(not_nzers)
    not_nzers = [x for x in not_nzers if getSingleUser(x[0]).location is None]
    removed = removed - len(not_nzers)
    print(removed)

    process_order = nzers + not_nzers
    """
    for z in [getSingleUser(x[0], dont_use_api=True) for x in process_order]:
        print(z.handle, z.id, z.follower_count + z.following_count)
    """

    count = 1
    max_count = len(ids_vs_connected_ids)
    for actor_id, _ in process_order:
        actor = getSingleUser(actor_id)
        print(f"{count}/{max_count}: Processing [{actor.handle}]...")
        completeCacheFile(actor_id)
        count += 1




import os
import pickle
import networkx as nx
import matplotlib.pyplot as plt
import datahandling
from file_loc import *
from globals import known_actors
from twitter_api import handleFromId, getSingleUser, TwitterAPIGenericException
import classes
import geo


nx_options = {
    'node_color': 'silver',
    'node_size': 4,
    'with_labels': True,
    'font_color': 'blue',
    'width': 1
}


def generateNetworkFromCache(remove_terminal_nodes=True, nz_only=False, ids_as_labels=False):
    full_dataset = set(os.listdir(followers_dir)).intersection(set(os.listdir(following_dir)))

    g = nx.Graph()

    # todo remove this eventually and populate nodes from all known actors, then only add edges based on mutuals
    edges = set()
    for actor_pkl in full_dataset:
        actor_id = actor_pkl.split(".")[0]
        datahandling.loadActor(actor_id)
        actor_username = known_actors[actor_id].handle
        with open(os.path.join(followers_dir, actor_pkl), "rb") as fi:
            followers = pickle.load(fi)
        with open(os.path.join(following_dir, actor_pkl), "rb") as fi:
            following = pickle.load(fi)
        #         for x in followers:
        # edges.add((actor_id, str(x), 'orange', 0.3)) # followed by
        for x in followers.intersection(following):
            edges.add((actor_id, str(x), 'sienna', 0.9))  # mutuals
            '''
            try:
                edges.add((actor_username, known_actors[str(x)].handle))
            except KeyError as e:
                edges.add((actor_username, str(x)))
                '''

    # todo improve
    nodes = set()
    for x in edges:
        nodes.add(x[0])
        nodes.add(x[1])
    nodes = list(nodes)  # keep the order consistent later

    is_new_zealand = dict()
    for x in nodes:
        try:
            actor = getSingleUser(x, dont_use_api=True)
        except TwitterAPIGenericException:
            is_new_zealand[x] = None
            continue
        is_new_zealand[x] = geo.isTwitterActorFromNewZealand(actor)

    node_names = {x: handleFromId(x, dont_use_api=True, return_id_on_failure=True) for x in nodes}
    if not ids_as_labels:
        nodes = [node_names[x] for x in nodes]
        edges = [(node_names[x[0]], node_names[x[1]], x[2], x[3]) for x in edges]

    g.add_nodes_from(nodes)

    # g.add_edges_from(edges)
    for x in edges:
        g.add_edge(x[0], x[1], color=x[2], alpha=x[3])

    # remove users who aren't known to be from NZ
    if nz_only:
        g.remove_nodes_from([node_names[k] for k, v in is_new_zealand.items() if v is None])

    # remove terminal nodes (people with only one friend)
    if remove_terminal_nodes:
        g.remove_nodes_from([x for x in g.nodes() if len(g.edges(x)) <= 1])

    return g


if __name__ == "__main__":


    '''
    ranked_by_degree_centrality = sorted(nx.degree_centrality(g).items(), key=lambda z: z[1], reverse=True)
    for x in ranked_by_degree_centrality:
        try:
            datahandling.loadActor(x[0])
            actor_name = known_actors[x[0]].handle
        except:
            actor_name = x[0]
        print(f"{actor_name}: {x[1]}")
    '''

    g = generateNetworkFromCache(remove_terminal_nodes=True, nz_only=False, ids_as_labels=True)

    sorted_by_location = {x: list() for x in [*geo.nz_cities, *geo.nz_regions, "no_location", "unknown", None]}
    ranked_by_total_mutuals_in_community = sorted([(x, len(g.edges(x))) for x in g.nodes()], key=lambda z: z[1], reverse=True)
    for x in ranked_by_total_mutuals_in_community:
        try:
            # getSingleUser(x[0])
            datahandling.loadActor(x[0])
            actor_name = known_actors[x[0]].handle
            if known_actors[x[0]].location:
                sorted_by_location[geo.isTwitterActorFromNewZealand(known_actors[x[0]])].append(actor_name)
            else:
                sorted_by_location["no_location"].append(actor_name)
        except:
            actor_name = x[0]
            sorted_by_location["unknown"].append(actor_name)


    total_actors = 0
    for location, actors_in_location in sorted_by_location.items():
        if not len(actors_in_location) or location in ["no_location", "unknown", None]:
            continue
        print(f"\n{location} ({len(actors_in_location)}):")
        total_actors += len(actors_in_location)
        for actor in actors_in_location:
            print(actor)

    print(f"\nThere are {total_actors} accounts mutually following at least 2 other accounts within this community, with location data implying presence in NZ.")
    print(f"Additionally, there are {len(sorted_by_location['no_location'])} accounts meeting this criteria with no location, and"
          f" {len(sorted_by_location['unknown'])} accounts that haven't been analysed yet.")


    if False:
        nx.draw(g,
                edge_color=nx.get_edge_attributes(g, 'color').values(),
                **nx_options)
        plt.show()






